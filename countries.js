import React, {Component} from 'react'
import {Segment} from 'semantic-ui-react'
import {observer} from 'mobx-react'


const Countries = observer(class Countries extends Component{

    render(){
        const{store} = this.props

        return(
            <Segment>
                {
                    store.countries.map(item=>{
                        return (
                            <div>{item.name}</div>
                        )
                    })
                }
                </Segment>

        )
    }


})

export default Countries