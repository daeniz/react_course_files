import { extendObservable, decorate, action, computed, configure } from 'mobx'

class CountryStore {

    constructor() {
        extendObservable(this, {
            countries: [],
            isLoading: false
        })

        this.loadCountries();

    }

    loadCountries() {
        this.isLoading = true
        fetch('https://restcountries.eu/rest/v2/all')
            .then(response => response.json())
            .then(result => {
                this.countries = result;
                this.isLoading = false;
            })
            .catch(error => error)
    }
}

decorate(CountryStore, {
    loadCountries: action
})

configure({ enforceActions: true });

export const countryStore = new CountryStore()